all:
	@echo Run \'make install\' to install Unattended Security Upgrades.

install:
	@mkdir --parents $(PREFIX)/usr/bin/
	@cp usu $(PREFIX)/usr/bin/usu
	@chmod +x $(PREFIX)/usr/bin/usu
	@cp usud $(PREFIX)/usr/bin/usud
	@chmod +x $(PREFIX)/usr/bin/usud
	@mkdir --parents $(PREFIX)/etc/init.d/
	@cp assets/usud.conf $(PREFIX)/etc/usud.conf
	@cp assets/usud $(PREFIX)/etc/init.d/usud
	@chmod +x $(PREFIX)/etc/init.d/usud
	@mkdir --parents $(PREFIX)/usr/lib/systemd/system/
	@cp assets/usud.service $(PREFIX)/usr/lib/systemd/system/

uninstall:
	@rm -f $(PREFIX)/usr/bin/usu
	@rm -f $(PREFIX)/usr/bin/usud
	@systemctl disable usud.service --now
	@rc-service usud stop
	@rc-update del usud
	@rm -f $(PREFIX)/usr/lib/systemd/system/usud.service
