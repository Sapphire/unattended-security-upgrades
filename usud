#!/bin/bash

if test -z "$DBUS_SESSION_BUS_ADDRESS" ; then
    eval `dbus-launch --sh-syntax`
    echo "$DBUS_SESSION_BUS_ADDRESS" > /tmp/usu-dbus
fi

usumsg() {
    echo -e "\033[1;32m$(date)\033[0m $1"
    logger -t "usud" "$1"
}

if [ "$EUID" != 0 ]; then
    usumsg "Run usud as root."
    exit 1
fi

usu() {
    usumsg "Upgrade Initiated"
    usumsg "Syncing Repositories"
    sudo pacman -Sy --noconfirm --noprogressbar
    usumsg "Querying insecure packages"
    declare -a vulnerable=($(arch-audit -q))
    declare -a allvulnerable=($(arch-audit -qr))
    declare -a upgradable=($(arch-audit -qur))
    usumsg "${#vulnerable[@]} packages have known security vulnerabilities."
    usumsg "$(echo ${#allvulnerable[@]}-${#vulnerable[@]} | bc) packages depend on packages that have security vulnerabilities."
    if [ $upgradetype == "full" ]; then
        declare -a fullupgrade=($(sudo pacman -Qu))
        usumsg "${#fullupgrade} packages will be upgraded regardless if they have security vulnerabilities or not."
        sudo pacman -Su --noconfirm --noprogressbar
        if [ $? != 0 ]; then
            usumsg "Something went wrong when trying to upgrade."
        else
            usumsg "Upgrade finished successfully."
        fi
    elif [ ${#upgradable[@]} != 0 ]; then
        usumsg "${#upgradable} potentially insecure packages will be upgraded."
        sudo pacman -S --noconfirm --noprogressbar ${upgradable[@]}
        if [ $? != 0 ]; then
            usumsg "Something went wrong when trying to upgrade."
        else
            usumsg "Upgrade finished successfully."
        fi
    else
        usumsg "No packages will be upgraded."
    fi
}

reloadconf() {
    metercheck=true # Default setting
    upgradetype=security # Default setting
    if [ ! -n "$USUD_CONF" ]; then
        usumsg "Using default configuration file."
        USUD_CONF="/etc/usud.conf"
    else
        usumsg "Using configuration file at $USUD_CONF"
    fi
    
    if [ -f "$USUD_CONF" ]; then
        source "$USUD_CONF"
        usumsg "Loaded configuration settings."
    else
        usumsg "The specified configuration file ($USUD_CONF) does not exist. We used the default settings instead."
    fi
}
reloadconf

usumsg "Daemon Started"
dbus-monitor "interface='com.sapphiregls.usud',member='control'" |
while read -r line; do
    usumsg "Received a DBus message, checking it..."
    if echo $line | grep -q /com/sapphiregls/usud/action/upgrade; then 
        usumsg "DBus message tells us to upgrade."
        if [ $metercheck == true ]; then
            usumsg "Checking if the internet connection is metered."
            if ! nmcli -t -f GENERAL.METERED dev show | grep -q "GENERAL.METERED:yes"; then
                usumsg "Connection is not metered. Initiating upgrade."
                usu
            else
                usumsg "Connection is metered. Not upgrading."
            fi
        else
            usumsg "Internet connection meter check is disabled, continuing."
            usu
        fi
    elif echo $line | grep -q /com/sapphiregls/usud/action/reload; then
        usumsg "DBus message tells us to reload the configuration file."
        reloadconf
    else
        usumsg "The DBus message did not tell us to do anything we could understand."
    fi
done
usumsg "dbus-monitor ended."
exit 1
