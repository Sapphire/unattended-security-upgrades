# unattended-security-upgrades

A daemon that upgrades packages that have security vulnerabilities and their dependencies when on an unmetered internet connection without interrupting work or shutdowns.